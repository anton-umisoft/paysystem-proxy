<?php
/**
 * Bootstrap Paysystem Proxy
 */

// Load configuration from file if it exist and readable.
$config = array_merge(
	is_readable($configFilePath = __DIR__ . '/config.ini') ? parse_ini_file($configFilePath) : array(),

	// Default configuration
	array(
		'dns' => 'mysql:host=localhost;dbname=payproxy',
		'username' => 'root',
		'password' => '',
		
		'yandex_shop_id' => 0000, // Идентификатор Контрагента, присваиваемый Оператором.
		'yandex_key' => 'secret', // Секретное слово Контрагента.
		'yandex_currency' => 10643, // Код валюты (По умолчанию, рубли: 10643).
		
		'shift_limit' => 1, // Колличество запрашиваемых из базы строк с запросами к backend-у.
		'backend_url' => 'http://umi.ru/custom/yandexCallback/', // URL по которому слать запросы на бекенд.
	)
);

$db = new PDO($config['dns'], $config['username'], $config['password']);

// Types of request in database.
define('REQUEST_PENDING', 'pending');
define('REQUEST_OK', 'ok');
define('REQUEST_ERROR', 'error');

// Function for working with database.

/**
 * Add row with request info to database. 
 * 
 * @param string $status 
 * @param array $request Raw request
 * @param DateTime $date Log time
 */
function push($status, array $request, DateTime $date) {
	global $db;

	$query = $db->prepare('INSERT INTO request_log (`status`, `request`, `date`) VALUES (:status, :request, :date)');
	$query->bindValue(':status', $status);
	$query->bindValue(':request', json_encode($request));
	$query->bindValue(':date', $date->format('Y-m-d H:i:s'));
	$query->execute();
}

/**
 * Shift requests from database.
 * 
 * @param int $limit
 * @return array
 */
function shift($limit = 1) {
	global $db;
	
	$query = $db->prepare('SELECT * FROM request_log WHERE `status` <> :ok ORDER BY `id` DESC LIMIT :limit');
	$query->bindValue(':ok', REQUEST_OK);
	$query->bindValue(':limit', $limit);
	$query->execute();
	
	return $query->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Update request status. 
 * 
 * @param int $id
 * @param string $status
 */
function mark($id, $status) {
	global $db;
	
	$query = $db->prepare('UPDATE request_log SET `status` = :status WHERE `id` = :id LIMIT 1');
	$query->bindValue(':id', $id, PDO::PARAM_INT);
	$query->bindValue(':status', $status);
	$query->execute();
}

<?php
/**
 * Gateway Paysystem Proxy
 * 
 * Take care of yandex callbacks.
 */

require_once __DIR__ . '/bootstrap.php';

// Данные от Яндекса
$action = getRequest('action'); // Тип запроса. Значение: "checkOrder" или "paymentAviso"
$invoiceId = getRequest('invoiceId'); // Уникальный номер транзакции в ИС Оператора.
$orderSumBankPaycash = getRequest('orderSumBankPaycash'); // Код процессингового центра Оператора для суммы заказа.
$customerNumber = getRequest('customerNumber'); // Идентификатор плательщика (присланный в платежной форме) на стороне Контрагента
$orderSumAmount = getRequest('orderSumAmount'); // Стоимость заказа.
$datetime = getRequest('requestDatetime'); // Момент формирования запроса в ИС Оператора.
$paymentPayerCode = getRequest('paymentPayerCode'); // Номер счета в ИС Оператора, с которого производится оплата.
$paymentType = getRequest('paymentType'); // Способ оплаты, присланный в платежной форме.
$orderId = getRequest('orderId'); // Номер заказа, присланный в платежной форме.
$checkSum = getRequest('md5'); // MD5-хэш параметров платежной формы

// Наши данные 
$shopId = $config['yandex_shop_id']; // Идентификатор Контрагента, присваиваемый Оператором.
$shopPassword = $config['yandex_key']; // Секретное слово Контрагента.
$orderSumCurrencyPaycash = $config['yandex_currency']; // Код валюты.

// Высчитываем и проверяем контрольную сумму 
$calculatedCheckSum = md5("$action;$orderSumAmount;$orderSumCurrencyPaycash;$orderSumBankPaycash;$shopId;$invoiceId;$customerNumber;$shopPassword");

if(strcasecmp($checkSum, $calculatedCheckSum) == 0) {
	// Логируем информацию о запросе.
	push(REQUEST_PENDING, $_REQUEST, new DateTime());	
	
	// Посылаем ответ об успешном запросе.
	sendYandexResponse(200, $invoiceId, $shopId, $action);
	
} else {
	// Сумма не верна, сообщяем об ошибке.
	sendYandexResponse(1, $invoiceId, $shopId, $action);
}

/**
 * Get parameter from request.
 * 
 * @param string $param
 * @param mixed $default
 * @return mixed
 */
function getRequest($param, $default = null) {
	return isset($_REQUEST[$param]) ? $_REQUEST[$param] : $default;
}

/**
 * Create XML response to Yandex, and send it. 
 * 
 * @param int $code
 * @param int $invoiceId
 * @param int $shopId
 * @param string $action Тип запроса. Значение: "checkOrder" или "paymentAviso". Мы проксируем только вторые.
 */
function sendYandexResponse($code, $invoiceId, $shopId, $action = 'paymentAviso') {
	$dom = new DOMDocument('1.0', 'UTF-8');
	$element = $dom->createElement($action . 'Response');
	$element->setAttribute('performedDatetime', date(DATE_ATOM));
	$element->setAttribute('invoiceId', $invoiceId);
	$element->setAttribute('shopId', $shopId);
	$element->setAttribute('code', $code); 
	$dom->appendChild($element);
	echo $dom->saveXML();
}

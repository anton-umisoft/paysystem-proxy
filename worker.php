<?php
/**
 * Paysystem Proxy Worker
 *
 * Get request from database and process them to backend.
 */

require_once __DIR__ . '/bootstrap.php';


foreach (shift($config['shift_limit']) as $row) {
	$id = $row['id'];
	$request = json_decode($row['request'], true);

	// Process request to backend.  
	$content = getContent($config['backend_url'], 'POST', $request);

	try {
		$response = new SimpleXMLElement($content);

		// Check response code.
		if ("200" === (string)$response->attributes()->code) {
			mark($id, REQUEST_OK);
		} else {
			mark($id, REQUEST_ERROR);
		}
		
	} catch (Exception $e) {
		mark($id, REQUEST_ERROR);
	}
}


/**
 * Returns the content fetched from a given URL.
 *
 * @param  string $url URL.
 * @param  string $method HTTP method to use for the request.
 * @param  mixed $data The data to send when doing non "get" requests.
 *                         Gets sent as POST parameters if a key/value
 *                         array is passed or as request body if a
 *                         string is passed.
 * @param  array $headers Additional headers to send with the request.
 *                         The headers array should look like this:
 *                         array(
 *                              'Content-type: text/plain',
 *                              'Content-length: 100'
 *                         )
 *
 * @return string
 */
function getContent($url, $method = 'GET', $data = array(), array $headers = array()) {
	$c = curl_init();

	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($c, CURLOPT_URL, $url);
	curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1); // Allow redirects.
	curl_setopt($c, CURLOPT_CUSTOMREQUEST, strtoupper($method)); // Define the HTTP method.

	if (!empty($data) && 'POST' === strtoupper($method)) {
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, http_build_query($data));
	}

	if (!empty($headers)) {
		curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
	}


	$content = curl_exec($c); // Execute the request
	curl_close($c);

	if (false === $content) {
		$content = null;
	}

	return $content;
}

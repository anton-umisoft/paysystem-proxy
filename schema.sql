CREATE TABLE `request_log` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `status` varchar(32) NOT NULL,
  `request` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE='InnoDB' COLLATE 'utf8_general_ci';
